
Media Field Display
-------------------
Provides display options for media fields. Currently only supports audio 
fields or link fields that point to audio files.

Based on a patch to mediafield by Jeff Eaton, http://drupal.org/node/120072

Installing
----------

To install, you will need to upload one or both of the following files
to the /players directory in this module's directory.

1pixelout.swf
button.swf

Both of these files are currently in the download for the audio module:

http://drupal.org/project/audio
