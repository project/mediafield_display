<?php

/**
 * @file
 * Provides display handlers for audio fields.
 *
 * When enabled, this module enables display alternatives for audio fields
 * provided by Media Field and link fields containing audio files. These
 * options are available in CCK field display administration and also in Views
 * field configuration.
 */

/**
 * Implementation of hook_field_formatter_info().
 */
function mediafield_display_field_formatter_info() {
  $formatters = array();
  if (module_exists('audiofield')) {
    $path = drupal_get_path('module', 'mediafield_display') .'/players/';
    if (file_exists($path .'1pixelout.swf')) {
      $formatters['1pixelout'] = array(
        'label' => t('1 Pixel Out player'),
        'field types' => array('file_audio', 'link'),
      );
      $formatters['1pixelout_plus'] = array(
        'label' => t('1 Pixel Out player plus download link'),
        'field types' => array('file_audio'),
      );
    }
    if (file_exists($path .'button.swf')) {
      $formatters['button'] = array(
        'label' => t('Button player'),
        'field types' => array('file_audio', 'link'),
      );
      $formatters['button_plus'] = array(
        'label' => t('Button player plus download link'),
        'field types' => array('file_audio'),
      );

    }
  }
  return $formatters;
}

/**
 * Implementation of hook_field_formatter().
 */
function mediafield_display_field_formatter($field, $item, $formatter) {
  global $base_url;

  switch ($field['type']) {
    case 'file_audio':
      require_once(drupal_get_path('module', 'audiofield') .'/multimediafile.inc');
      if (!isset($item['fid']) || empty($item['fid'])) {
        return '';
      }
      $file = _field_file_load($item['fid']);
      $file_url = check_url($base_url .'/'. $file['filepath']);
      $file_title = check_plain($file['description']);
      $attributes = array();
      $query = $fragment = NULL;
      break;
    case 'link':
      if (empty($item['url']) && ($field['url'] != 'optional' || empty($item['title']))) {
        return '';
      }

      $attributes = array();
      $item['attributes'] = unserialize($item['attributes']);
      // Add attributes defined at the widget level
      if (is_array($item['attributes'])) {
        foreach($item['attributes'] as $attribute => $attbvalue) {
          if (isset($item['attributes'][$attribute]) && $field['attributes'][$attribute] == 'user') {
            $attributes[$attribute] = $attbvalue;
          }
        }
      }
      // Add attributes defined at the field level
      if (is_array($field['attributes'])) {
        foreach($field['attributes'] as $attribute => $attbvalue) {
          if (!empty($attbvalue) && $attbvalue != 'default' && $attbvalue != 'user') {
            $attributes[$attribute] = $attbvalue;
          }
        }
      }

      // Replace URL tokens
      if (module_exists('token') && $field['enable_tokens']) {
        $item['url'] = token_replace($item['url'], 'node', $node);
      }

      $type = link_validate_url($item['url']);
      $file_url = $url = link_cleanup_url($item['url']);
      
      // Separate out the anchor if any
      if (strpos($url, '#') !== FALSE) {
        $fragment = substr($url, strpos($url, '#') + 1);
        $url = substr($url, 0, strpos($url, '#'));
      }
      // Separate out the query string if any
      if (strpos($url, '?') !== FALSE) {
        $query = substr($url, strpos($url, '?') + 1);
        $url = substr($url, 0, strpos($url, '?'));
      }

      // Build the title
      if (strlen(trim($item['title'])) || ($field['title'] == 'value' && strlen(trim($field['title_value'])))) {
        // Use the title defined at the field level
        if ($field['title'] == 'value' && strlen(trim($field['title_value']))) {
          $file_title = $field['title_value'];
        }
        // Use the title defined by the user at the widget level
        else {
          $file_title = $item['title'];
        }
        // Replace tokens
        if (module_exists('token') && ($field['title'] == 'value' || $field['enable_tokens'])) {
          $file_title = token_replace($file_title, 'node', $node);
        }

        $file_title = check_plain($file_title);
      }

      break;
  }

  switch ($formatter) {
    case '1pixelout':
    case 'button':
      $output = theme('mediafield_display_'. $formatter, $file_title, $file_url, $item, $field);
      break;
    case '1pixelout_plus':
    case 'button_plus':
      $output = theme('mediafield_display_'. substr($formatter, 0, strlen($formatter) - 5), $file_title, $file_url, $item, $field);
      $output .= l(t('Download'), $file_url, $attributes, $query, $fragment);
      break;
  }

  return $output;
}

/**
 * Theme a 1pixelout audio file.
 */
function theme_mediafield_display_1pixelout($file_title, $file_url, $item, $field) {
  global $base_url;

  // Set the URL for the player.
  $url = $base_url . '/' . drupal_get_path('module', 'mediafield_display') .
         '/players/1pixelout.swf';

  // Set some Flash variables.
  $options = array();
  $options['soundFile'] = $file_url;
  $flashvars = drupal_query_string_encode($options);

  // This is a fix for http://drupal.org/node/158687.
  $flashvars = str_replace('http%3A/%252F', 'http://', $flashvars);

$output = <<<EOT
<object type="application/x-shockwave-flash" data="$url" width="290" height="24">
  <param name="movie" value="$url" />
  <param name="wmode" value="transparent" />
  <param name="menu" value="false" />
  <param name="quality" value="high" />
  <param name="FlashVars" value="$flashvars" />
</object>
EOT;

  return $output;
}

/**
 * Theme a button audio file.
 */
function theme_mediafield_display_button($file_title, $file_url, $item, $field) {
  global $base_url;

  $options = array();
  $options['song_url'] = $file_url;
  $options['song_title'] = check_plain($file_title);
  // str_replace() is to fix issue in Drupal 5.2.
  $url = $base_url .'/'. drupal_get_path('module', 'mediafield_display') .'/players/button.swf?'. str_replace('http%3A/%252F', 'http://', drupal_query_string_encode($options));

$output = <<<EOT
<object type="application/x-shockwave-flash" data="$url" width="17" height="17">';
  <param name="movie" value="$url" />
  <param name="wmode" value="transparent" />
  <param name="quality" value="high" />
</object>
EOT;

  return $output;
}
